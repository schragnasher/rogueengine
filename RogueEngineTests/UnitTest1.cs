﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK;
using RogueEngine.SceneManagement;

namespace RogueEngineTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void NodePositionTest()
        {
            var position = new Vector3(10, 10, 10);
            var node = new Node();

            node.Position = position;

            Assert.AreEqual(position, node.LocalPosition);
            Assert.AreEqual(position, node.Position);
        }

        [TestMethod]
        public void NodePositionHierarchy()
        {
            var parentPosition = new Vector3(5, 5, 5);
            var childPosition = new Vector3(-5, -5, -5);
            var childWorldPosition = new Vector3(-10, -10, -10);
            var parentNode = new Node();
            var childNode = new Node();

            parentNode.AddChild(childNode);

            parentNode.Position = parentPosition;
            childNode.Position = childPosition;

            Assert.AreEqual(childPosition, childNode.Position);
            Assert.AreEqual(childWorldPosition, childNode.LocalPosition);
            Assert.AreEqual(parentPosition, parentNode.Position);
            Assert.AreEqual(parentPosition, parentNode.LocalPosition);
        }

        [TestMethod]
        public void NodeRotationTest()
        {
            var rotation = Quaternion.FromAxisAngle(Vector3.UnitX, 90.0f / 180.0f);
            var node = new Node();

            node.Rotation = rotation;

            Assert.AreEqual(rotation, node.LocalRotation);
            Assert.AreEqual(rotation, node.Rotation);
        }

        [TestMethod]
        public void NodeRotationHierarchy()
        {
            var parentRotation = Quaternion.FromAxisAngle(Vector3.UnitX, MathHelper.DegreesToRadians(41.0f)).Normalized();
            var childRotation = Quaternion.FromAxisAngle(Vector3.UnitX, MathHelper.DegreesToRadians(23.0f)).Normalized();
            var parentNode = new Node();
            var childNode = new Node();

            parentNode.AddChild(childNode);

            parentNode.Rotation = parentRotation;
            childNode.Rotation = childRotation;

            Assert.IsTrue((parentRotation.X - parentNode.Rotation.X) < 0.0001f);
            Assert.IsTrue((parentRotation.Y - parentNode.Rotation.Y) < 0.0001f);
            Assert.IsTrue((parentRotation.Z - parentNode.Rotation.Z) < 0.0001f);
            Assert.IsTrue((parentRotation.W - parentNode.Rotation.W) < 0.0001f);
            Assert.IsTrue((childRotation.X - childNode.Rotation.X) < 0.0001f);
            Assert.IsTrue((childRotation.Y - childNode.Rotation.Y) < 0.0001f);
            Assert.IsTrue((childRotation.Z - childNode.Rotation.Z) < 0.0001f);
            Assert.IsTrue((childRotation.W - childNode.Rotation.W) < 0.0001f);
        }
    }
}