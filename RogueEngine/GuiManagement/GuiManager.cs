﻿using Noesis;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;

namespace RogueEngine.GuiManagement
{
    [Export]
    public class GuiManager
    {
        private readonly RogueGame _game;
        private readonly RenderDeviceGL _renderdevice;

        private List<View> _views;
        private Dictionary<OpenTK.Input.Key, Key> _keyLookup;

        private double _time;

        [ImportingConstructor]
        public GuiManager(RogueGame game)
        {
            _game = game;

            _renderdevice = new RenderDeviceGL();
            _views = new List<View>();
            _keyLookup = new Dictionary<OpenTK.Input.Key, Key>();

            SetupKeyboardLookup();
        }

        public void Initialize()
        {
            GUI.SetLicense("Joshua Baker", "q10b7VEUG+2YeVhS7ihVBRM3ADjkfdJRhkEGqIAE5HfpITvU");
            GUI.Init();
            NoesisApp.Application.SetThemeProviders();
            GUI.LoadApplicationResources("Theme/NoesisTheme.DarkBlue.xaml");
            GUI.SetXamlProvider(new FileXamlProvider());

            _game.MouseMove += OnMouseMove;
            _game.MouseDown += OnMouseButtonDown;
            _game.MouseUp += OnMouseButtonUp;
            _game.MouseWheel += OnMouseWheel;
            _game.KeyDown += OnKeyDown;
            _game.KeyUp += OnKeyUp;
            _game.KeyPress += OnKeyPress;
            _game.Resize += OnWindowResize;
        }

        public void Update(double dt)
        {
            _time += dt;

            foreach (var view in _views)
            {
                view.Update(_time);
            }
        }

        public void Render()
        {
            foreach (var view in _views)
            {
                view.Renderer.UpdateRenderTree();
                view.Renderer.RenderOffscreen();
                view.Renderer.Render();
            }

            GL.DepthMask(true);
        }

        public void Shutdown()
        {
            GUI.UnregisterNativeTypes();
            GUI.Shutdown();
        }

        public View CreateView(string filePath)
        {
            var root = GUI.LoadXaml(filePath) as FrameworkElement;
            var view = GUI.CreateView(root);

            view.SetSize(_game.Width, _game.Height);
            view.Renderer.Init(_renderdevice);

            _views.Add(view);

            return view;
        }

        #region private

        private void SetupKeyboardLookup()
        {
            _keyLookup.Add(OpenTK.Input.Key.A, Key.A);
            _keyLookup.Add(OpenTK.Input.Key.B, Key.B);
            _keyLookup.Add(OpenTK.Input.Key.C, Key.C);
            _keyLookup.Add(OpenTK.Input.Key.D, Key.D);
            _keyLookup.Add(OpenTK.Input.Key.E, Key.E);
            _keyLookup.Add(OpenTK.Input.Key.F, Key.F);
            _keyLookup.Add(OpenTK.Input.Key.G, Key.G);
            _keyLookup.Add(OpenTK.Input.Key.H, Key.H);
            _keyLookup.Add(OpenTK.Input.Key.I, Key.I);
            _keyLookup.Add(OpenTK.Input.Key.J, Key.J);
            _keyLookup.Add(OpenTK.Input.Key.K, Key.K);
            _keyLookup.Add(OpenTK.Input.Key.L, Key.L);
            _keyLookup.Add(OpenTK.Input.Key.M, Key.M);
            _keyLookup.Add(OpenTK.Input.Key.N, Key.N);
            _keyLookup.Add(OpenTK.Input.Key.O, Key.O);
            _keyLookup.Add(OpenTK.Input.Key.P, Key.P);
            _keyLookup.Add(OpenTK.Input.Key.Q, Key.Q);
            _keyLookup.Add(OpenTK.Input.Key.R, Key.R);
            _keyLookup.Add(OpenTK.Input.Key.S, Key.S);
            _keyLookup.Add(OpenTK.Input.Key.T, Key.T);
            _keyLookup.Add(OpenTK.Input.Key.U, Key.U);
            _keyLookup.Add(OpenTK.Input.Key.V, Key.V);
            _keyLookup.Add(OpenTK.Input.Key.W, Key.W);
            _keyLookup.Add(OpenTK.Input.Key.X, Key.X);
            _keyLookup.Add(OpenTK.Input.Key.Y, Key.Y);
            _keyLookup.Add(OpenTK.Input.Key.Z, Key.Z);
            _keyLookup.Add(OpenTK.Input.Key.Number0, Key.D0);
            _keyLookup.Add(OpenTK.Input.Key.Number1, Key.D1);
            _keyLookup.Add(OpenTK.Input.Key.Number2, Key.D2);
            _keyLookup.Add(OpenTK.Input.Key.Number3, Key.D3);
            _keyLookup.Add(OpenTK.Input.Key.Number4, Key.D4);
            _keyLookup.Add(OpenTK.Input.Key.Number5, Key.D5);
            _keyLookup.Add(OpenTK.Input.Key.Number6, Key.D6);
            _keyLookup.Add(OpenTK.Input.Key.Number7, Key.D7);
            _keyLookup.Add(OpenTK.Input.Key.Number8, Key.D8);
            _keyLookup.Add(OpenTK.Input.Key.Number9, Key.D9);
            _keyLookup.Add(OpenTK.Input.Key.F1, Key.F1);
            _keyLookup.Add(OpenTK.Input.Key.F2, Key.F2);
            _keyLookup.Add(OpenTK.Input.Key.F3, Key.F3);
            _keyLookup.Add(OpenTK.Input.Key.F4, Key.F4);
            _keyLookup.Add(OpenTK.Input.Key.F5, Key.F5);
            _keyLookup.Add(OpenTK.Input.Key.F6, Key.F6);
            _keyLookup.Add(OpenTK.Input.Key.F7, Key.F7);
            _keyLookup.Add(OpenTK.Input.Key.F8, Key.F8);
            _keyLookup.Add(OpenTK.Input.Key.F9, Key.F9);
            _keyLookup.Add(OpenTK.Input.Key.F10, Key.F10);
            _keyLookup.Add(OpenTK.Input.Key.F11, Key.F11);
            _keyLookup.Add(OpenTK.Input.Key.F12, Key.F12);
            _keyLookup.Add(OpenTK.Input.Key.F13, Key.F13);
            _keyLookup.Add(OpenTK.Input.Key.F14, Key.F14);
            _keyLookup.Add(OpenTK.Input.Key.F15, Key.F15);
            _keyLookup.Add(OpenTK.Input.Key.F16, Key.F16);
            _keyLookup.Add(OpenTK.Input.Key.F17, Key.F17);
            _keyLookup.Add(OpenTK.Input.Key.F18, Key.F18);
            _keyLookup.Add(OpenTK.Input.Key.F19, Key.F19);
            _keyLookup.Add(OpenTK.Input.Key.F20, Key.F20);
            _keyLookup.Add(OpenTK.Input.Key.F21, Key.F21);
            _keyLookup.Add(OpenTK.Input.Key.F22, Key.F22);
            _keyLookup.Add(OpenTK.Input.Key.F23, Key.F23);
            _keyLookup.Add(OpenTK.Input.Key.F24, Key.F24);
            _keyLookup.Add(OpenTK.Input.Key.Up, Key.Up);
            _keyLookup.Add(OpenTK.Input.Key.Down, Key.Down);
            _keyLookup.Add(OpenTK.Input.Key.Left, Key.Left);
            _keyLookup.Add(OpenTK.Input.Key.Right, Key.Right);
            _keyLookup.Add(OpenTK.Input.Key.Enter, Key.Enter);
            _keyLookup.Add(OpenTK.Input.Key.Escape, Key.Escape);
            _keyLookup.Add(OpenTK.Input.Key.Space, Key.Space);
            _keyLookup.Add(OpenTK.Input.Key.Tab, Key.Tab);
            _keyLookup.Add(OpenTK.Input.Key.BackSpace, Key.Back);
            _keyLookup.Add(OpenTK.Input.Key.NumLock, Key.NumLock);
            _keyLookup.Add(OpenTK.Input.Key.Keypad0, Key.NumPad0);
            _keyLookup.Add(OpenTK.Input.Key.Keypad1, Key.NumPad1);
            _keyLookup.Add(OpenTK.Input.Key.Keypad2, Key.NumPad2);
            _keyLookup.Add(OpenTK.Input.Key.Keypad3, Key.NumPad3);
            _keyLookup.Add(OpenTK.Input.Key.Keypad4, Key.NumPad4);
            _keyLookup.Add(OpenTK.Input.Key.Keypad5, Key.NumPad5);
            _keyLookup.Add(OpenTK.Input.Key.Keypad6, Key.NumPad6);
            _keyLookup.Add(OpenTK.Input.Key.Keypad7, Key.NumPad7);
            _keyLookup.Add(OpenTK.Input.Key.Keypad8, Key.NumPad8);
            _keyLookup.Add(OpenTK.Input.Key.Keypad9, Key.NumPad9);
            _keyLookup.Add(OpenTK.Input.Key.KeypadDivide, Key.Divide);
            _keyLookup.Add(OpenTK.Input.Key.KeypadMultiply, Key.Multiply);
            _keyLookup.Add(OpenTK.Input.Key.KeypadSubtract, Key.Subtract);
            _keyLookup.Add(OpenTK.Input.Key.KeypadAdd, Key.Add);
            _keyLookup.Add(OpenTK.Input.Key.KeypadDecimal, Key.Decimal);
            _keyLookup.Add(OpenTK.Input.Key.KeypadEnter, Key.Enter);
            _keyLookup.Add(OpenTK.Input.Key.ShiftLeft, Key.LeftShift);
            _keyLookup.Add(OpenTK.Input.Key.ShiftRight, Key.RightShift);
            _keyLookup.Add(OpenTK.Input.Key.ControlLeft, Key.LeftCtrl);
            _keyLookup.Add(OpenTK.Input.Key.ControlRight, Key.RightCtrl);
            _keyLookup.Add(OpenTK.Input.Key.AltLeft, Key.LeftAlt);
            _keyLookup.Add(OpenTK.Input.Key.AltRight, Key.RightAlt);
            _keyLookup.Add(OpenTK.Input.Key.WinLeft, Key.LWin);
            _keyLookup.Add(OpenTK.Input.Key.WinRight, Key.RWin);
            _keyLookup.Add(OpenTK.Input.Key.Insert, Key.Insert);
            _keyLookup.Add(OpenTK.Input.Key.Delete, Key.Delete);
            _keyLookup.Add(OpenTK.Input.Key.PageUp, Key.PageUp);
            _keyLookup.Add(OpenTK.Input.Key.PageDown, Key.PageDown);
            _keyLookup.Add(OpenTK.Input.Key.Home, Key.Home);
            _keyLookup.Add(OpenTK.Input.Key.End, Key.End);
            _keyLookup.Add(OpenTK.Input.Key.CapsLock, Key.CapsLock);
            _keyLookup.Add(OpenTK.Input.Key.ScrollLock, Key.Scroll);
            _keyLookup.Add(OpenTK.Input.Key.PrintScreen, Key.PrintScreen);
            _keyLookup.Add(OpenTK.Input.Key.Pause, Key.Pause);
            _keyLookup.Add(OpenTK.Input.Key.Minus, Key.OemMinus);
            _keyLookup.Add(OpenTK.Input.Key.Plus, Key.OemPlus);
            _keyLookup.Add(OpenTK.Input.Key.BracketLeft, Key.OemOpenBrackets);
            _keyLookup.Add(OpenTK.Input.Key.BracketRight, Key.OemCloseBrackets);
            _keyLookup.Add(OpenTK.Input.Key.Semicolon, Key.OemSemicolon);
            _keyLookup.Add(OpenTK.Input.Key.Quote, Key.OemQuotes);
            _keyLookup.Add(OpenTK.Input.Key.Comma, Key.OemComma);
            _keyLookup.Add(OpenTK.Input.Key.Period, Key.OemPeriod);
            _keyLookup.Add(OpenTK.Input.Key.Slash, Key.OemQuestion);
            _keyLookup.Add(OpenTK.Input.Key.BackSlash, Key.OemBackslash);
            _keyLookup.Add(OpenTK.Input.Key.Tilde, Key.OemTilde);
            _keyLookup.Add(OpenTK.Input.Key.Clear, Key.Clear);

            //_keyLookup.Add(OpenTK.Input.Key.NonUSBackSlash, Noesis.Key.A);
            //_keyLookup.Add(OpenTK.Input.Key.Unknown, Noesis.Key);
            //_keyLookup.Add(OpenTK.Input.Key.Menu, Noesis.Key.A);
            //_keyLookup.Add(OpenTK.Input.Key.Sleep, Noesis.Key.A);
            //_keyLookup.Add(OpenTK.Input.Key.LastKey, Noesis.Key.A);
        }

        private void OnWindowResize(object sender, System.EventArgs e)
        {
            foreach (var view in _views)
            {
                view.SetSize(_game.Width, _game.Height);
            }
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            foreach (var view in _views)
            {
                view.Char(e.KeyChar);
            }
        }

        private void OnKeyUp(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {
            if (_keyLookup.ContainsKey(e.Key))
            {
                foreach (var view in _views)
                {
                    view.KeyUp(_keyLookup[e.Key]);
                }
            }
        }

        private void OnKeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {
            if (_keyLookup.ContainsKey(e.Key))
            {
                foreach (var view in _views)
                {
                    view.KeyDown(_keyLookup[e.Key]);
                }
            }
        }

        private void OnMouseWheel(object sender, OpenTK.Input.MouseWheelEventArgs e)
        {
            foreach (var view in _views)
            {
                view.MouseWheel(e.X, e.Y, e.Delta);
            }
        }

        private void OnMouseButtonUp(object sender, OpenTK.Input.MouseButtonEventArgs e)
        {
            foreach (var view in _views)
            {
                view.MouseButtonUp(e.X, e.Y, MouseButton.Left);
            }
        }

        private void OnMouseButtonDown(object sender, OpenTK.Input.MouseButtonEventArgs e)
        {
            foreach (var view in _views)
            {
                view.MouseButtonDown(e.X, e.Y, MouseButton.Left);
            }
        }

        private void OnMouseMove(object sender, OpenTK.Input.MouseMoveEventArgs e)
        {
            foreach (var view in _views)
            {
                view.MouseMove(e.X, e.Y);
            }
        }

        #endregion private
    }
}