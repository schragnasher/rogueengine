﻿using Noesis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RogueEngine.GuiManagement
{
    public class FileXamlProvider : XamlProvider
    {
        public FileXamlProvider()
        {
        }

        public override Stream LoadXaml(Uri uri)
        {
            var filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, uri.OriginalString);

            return File.OpenRead(filePath);
        }
    }
}