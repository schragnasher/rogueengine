﻿using System.ComponentModel.Composition;

namespace RogueEngine.StageManagement
{
    [Export]
    public class StageManager
    {
        [ImportingConstructor]
        public StageManager()
        {
        }

        #region public

        public void LoadStage(Stage stage)
        {
            stage.Load();
        }

        public void LoadStageAdditive(Stage stage)
        {
            stage.Load();
        }

        #endregion public
    }
}