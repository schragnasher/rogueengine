﻿namespace RogueEngine.StageManagement
{
    #region public

    public abstract class Stage
    {
        public abstract void Load();
    }

    #endregion public
}