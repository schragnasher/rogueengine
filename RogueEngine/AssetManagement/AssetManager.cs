﻿using log4net;
using OpenTK.Graphics.OpenGL;
using RogueEngine.AssetManagement.Assets;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Reflection;

namespace RogueEngine.AssetManagement
{
    [Export]
    public class AssetManager
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private const string AssetsPath = "Assets";

        private Dictionary<string, object> _assets;

        [ImportingConstructor]
        public AssetManager()
        {
            _assets = new Dictionary<string, object>();
        }

        #region public

        public void Initialize()
        {
        }

        public object Load<T>(string assetName)
        {
            object asset = null;

            //check if this asset is already loaded
            if (_assets.ContainsKey(assetName))
            {
                asset = _assets[assetName];
            }
            else
            {
                //build path and check it exists
                var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AssetsPath, assetName);

                //load asset
                if (typeof(T) == typeof(ShaderProgram))
                {
                    asset = LoadShaderProgram(assetName);
                }
                else if (typeof(T) == typeof(Model))
                {
                    asset = Model.Load(filePath);

                    _log.Debug($"Model Loaded: {filePath}");
                }
                else if (typeof(T) == typeof(Texture2d))
                {
                    asset = Texture2d.Load(filePath);
                }
            }

            return asset;
        }

        public void Shutdown()
        {
        }

        #endregion public

        #region private

        private ShaderProgram LoadShaderProgram(string name)
        {
            var vertexShaderPath = AssetsPath + "/" + name + ".vert";
            var fragmentShaderPath = AssetsPath + "/" + name + ".frag";

            var vertexShader = new Shader(ShaderType.VertexShader, File.ReadAllText(vertexShaderPath));
            var fragmentShader = new Shader(ShaderType.FragmentShader, File.ReadAllText(fragmentShaderPath));

            _log.Debug("Shader Loaded: " + name);

            return new ShaderProgram(vertexShader, fragmentShader);
        }

        #endregion private
    }
}