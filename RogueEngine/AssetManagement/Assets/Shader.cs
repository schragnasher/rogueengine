﻿using log4net;
using OpenTK.Graphics.OpenGL;
using System.Reflection;

namespace RogueEngine.AssetManagement.Assets
{
    public class Shader
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Shader(ShaderType type, string sourceCode)
        {
            Id = GL.CreateShader(type);

            GL.ShaderSource(Id, sourceCode);

            GL.CompileShader(Id);

            var info = GL.GetShaderInfoLog(Id);

            if (!string.IsNullOrEmpty(info))
            {
                _log.Debug(GL.GetShaderInfoLog(Id));
            }
        }

        public int Id { get; }
    }
}