﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RogueEngine.AssetManagement.Assets
{
    public class Material
    {
        public ShaderProgram Shader { get; set; }

        public Texture2d[] Textures { get; }

        public Material(ShaderProgram shader)
        {
            Shader = shader;

            Textures = new Texture2d[16];
        }
    }
}