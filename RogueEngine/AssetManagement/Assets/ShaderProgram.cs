﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace RogueEngine.AssetManagement.Assets
{
    public class ShaderProgram
    {
        public int Id { get; private set; }

        public ShaderProgram(params Shader[] shaders)
        {
            Id = GL.CreateProgram();

            foreach (var shader in shaders)
            {
                GL.AttachShader(Id, shader.Id);
            }

            GL.LinkProgram(Id);

            foreach (var shader in shaders)
            {
                GL.DetachShader(Id, shader.Id);
            }
        }

        public void UseProgram()
        {
            GL.UseProgram(Id);
        }

        public int GetUniform(string name)
        {
            return GL.GetUniformLocation(Id, name);
        }

        public void SetUniform(int id, Matrix4 value)
        {
            GL.UniformMatrix4(id, false, ref value);
        }

        public void SetUniform(int id, int value)
        {
            GL.Uniform1(id, value);
        }

        public void SetUniform(int id, float value)
        {
            GL.Uniform1(id, value);
        }

        public void SetUniform(int id, double value)
        {
            GL.Uniform1(id, value);
        }

        public void SetUniform(int id, Vector3 value)
        {
            GL.Uniform3(id, value);
        }
    }
}