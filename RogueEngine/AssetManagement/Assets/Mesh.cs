﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace RogueEngine.AssetManagement.Assets
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Vertex
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TexCoords;
        public Vector3 Tangent;
        public Vector3 BiTangent;
    }

    public class Mesh
    {
        private uint _vertexArrayObject;
        private uint _vertexBufferObject;
        private uint _indexBufferObject;
        private Vertex[] _vertices;
        private uint[] _indices;

        internal Mesh(Vertex[] vertices, uint[] indices)
        {
            _vertices = vertices;
            _indices = indices;

            Initialize();
        }

        #region public

        public int MaterialIndex { get; internal set; }

        public void Render()
        {
            GL.BindVertexArray(_vertexArrayObject);

            GL.DrawElements(PrimitiveType.Triangles, _indices.Length, DrawElementsType.UnsignedInt, 0);

            GL.BindVertexArray(0);
        }

        #endregion public

        #region private

        internal void Initialize()
        {
            //create buffers for mesh
            GL.CreateVertexArrays(1, out _vertexArrayObject);
            GL.CreateBuffers(1, out _vertexBufferObject);
            GL.CreateBuffers(1, out _indexBufferObject);

            //bind vertex array
            GL.BindVertexArray(_vertexArrayObject);

            //load vertices
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, _vertices.Length * Marshal.SizeOf(typeof(Vertex)), _vertices, BufferUsageHint.StaticDraw);

            //load indices
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _indexBufferObject);
            GL.BufferData(BufferTarget.ElementArrayBuffer, _indices.Length * sizeof(uint), _indices, BufferUsageHint.StaticDraw);

            //setup vertex position pointer
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)), 0);

            //setup vertex normal pointer
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)), Marshal.OffsetOf(typeof(Vertex), "Normal"));

            //setup texture coordinate pointer
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)), Marshal.OffsetOf(typeof(Vertex), "TexCoords"));

            //setup tangent pointer
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)), Marshal.OffsetOf(typeof(Vertex), "Tangent"));

            //setup bittangent pointer
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)), Marshal.OffsetOf(typeof(Vertex), "BiTangent"));

            GL.EnableVertexAttribArray(0);
        }

        #endregion private

        #region internal

        internal static Mesh Load(Assimp.Mesh aiMesh)
        {
            //process vertices
            var vertices = new List<Vertex>();
            for (int i = 0; i < aiMesh.VertexCount; i++)
            {
                var vertex = new Vertex();
                vertex.Position = new Vector3(aiMesh.Vertices[i].X, aiMesh.Vertices[i].Y, aiMesh.Vertices[i].Z);
                vertex.Normal = new Vector3(aiMesh.Normals[i].X, aiMesh.Normals[i].Y, aiMesh.Normals[i].Z);

                if (aiMesh.HasTextureCoords(0))
                {
                    vertex.TexCoords = new Vector2(aiMesh.TextureCoordinateChannels[0][i].X, aiMesh.TextureCoordinateChannels[0][i].Y);
                }

                if (aiMesh.HasTangentBasis)
                {
                    vertex.Tangent = new Vector3(aiMesh.Tangents[i].X, aiMesh.Tangents[i].Y, aiMesh.Tangents[i].Z);
                    vertex.BiTangent = new Vector3(aiMesh.BiTangents[i].X, aiMesh.BiTangents[i].Y, aiMesh.BiTangents[i].Z);
                }

                vertices.Add(vertex);
            }

            var mesh = new Mesh(vertices.ToArray(), aiMesh.GetUnsignedIndices());

            mesh.MaterialIndex = aiMesh.MaterialIndex;

            return mesh;
        }

        #endregion internal
    }
}