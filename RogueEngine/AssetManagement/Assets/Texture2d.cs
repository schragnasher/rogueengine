﻿using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Drawing.Imaging;

namespace RogueEngine.AssetManagement.Assets
{
    public class Texture2d
    {
        internal int Id { get; private set; }

        internal Texture2d(int textureId)
        {
            Id = textureId;
        }

        internal void Bind()
        {
            GL.BindTexture(TextureTarget.Texture2D, Id);
        }

        internal static Texture2d Load(string filePath)
        {
            var textureId = GL.GenTexture();
            var texture = new Texture2d(textureId);

            GL.BindTexture(TextureTarget.Texture2D, textureId);

            var bitmap = new Bitmap(filePath);

            var bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(
                TextureTarget.Texture2D,
                0,
                PixelInternalFormat.Rgba,
                bitmapData.Width,
                bitmapData.Height,
                0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
                PixelType.UnsignedByte,
                bitmapData.Scan0);

            bitmap.UnlockBits(bitmapData);

            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            return texture;
        }
    }
}