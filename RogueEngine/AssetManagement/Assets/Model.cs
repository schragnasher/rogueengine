﻿using Assimp;
using Assimp.Configs;
using RogueEngine.Utility;
using System.Collections.Generic;

namespace RogueEngine.AssetManagement.Assets
{
    public class Model
    {
        internal Model(List<Mesh> meshes = null, List<Material> materials = null)
        {
            Meshes = meshes != null ? new List<Mesh>(meshes) : new List<Mesh>(); ;
            Materials = materials != null ? new List<Material>(materials) : new List<Material>();
        }

        #region public

        public List<Material> Materials { get; }

        public List<Mesh> Meshes { get; }

        #endregion public

        #region internal

        internal static Model Load(string filePath)
        {
            var assetManager = ServiceLocator.GetInstance<AssetManager>();

            //config assimp for import
            var importer = new AssimpContext();
            importer.SetConfig(new NormalSmoothingAngleConfig(66.0f));

            //load scene from assimp
            var scene = importer.ImportFile(filePath,
                PostProcessSteps.Triangulate |
                PostProcessSteps.FlipUVs |
                PostProcessSteps.CalculateTangentSpace |
                PostProcessSteps.GenerateUVCoords |
                PostProcessSteps.GenerateNormals);

            //convert all meshes
            var meshes = new List<Mesh>();
            foreach (var mesh in scene.Meshes)
            {
                meshes.Add(Mesh.Load(mesh));
            }

            //return model
            return new Model(meshes);
        }

        #endregion internal
    }
}