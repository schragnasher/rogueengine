﻿using OpenTK;
using RogueEngine.SceneManagement.Nodes;

namespace RogueEngine.SceneManagement
{
    public abstract class Renderable
    {
        #region public

        public abstract void Render(Matrix4 model, Matrix4 view, Matrix4 projection, CameraNode camera, LightNode[] lights);

        #endregion public
    }
}