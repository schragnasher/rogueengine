﻿using OpenTK;
using RogueEngine.SceneManagement.Nodes;
using System.Collections.Generic;

namespace RogueEngine.SceneManagement
{
    public class Node
    {
        private List<Node> _children;
        private List<Renderable> _renderables;

        public Node()
        {
            _children = new List<Node>();
            _renderables = new List<Renderable>();

            LocalRotation = Quaternion.Identity;
            LocalScale = Vector3.One;
        }

        #region public

        #region properties

        public Node Parent { get; private set; }

        public Vector3 Left
        {
            get
            {
                return WorldTransform.Row0.Xyz;
            }
        }

        public Vector3 Right
        {
            get
            {
                return -Left;
            }
        }

        public Vector3 Up
        {
            get
            {
                return WorldTransform.Row1.Xyz;
            }
        }

        public Vector3 Down
        {
            get
            {
                return -Up;
            }
        }

        public Vector3 Forward
        {
            get
            {
                return WorldTransform.Row2.Xyz;
            }
        }

        public Vector3 Backward
        {
            get
            {
                return -Forward;
            }
        }

        public Vector3 Position
        {
            get
            {
                return Vector3.TransformPosition(LocalPosition, WorldMatrix);
            }

            set
            {
                LocalPosition = Vector3.TransformPosition(value, WorldMatrix.Inverted());
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return WorldTransform.ExtractRotation();
            }

            set
            {
                LocalRotation = (Matrix4.CreateFromQuaternion(value) * WorldMatrix.Inverted()).ExtractRotation();
            }
        }

        public Vector3 Scale { get; set; }

        public Vector3 LocalPosition { get; set; }

        public Quaternion LocalRotation { get; set; }

        public Vector3 LocalScale { get; set; }

        public Matrix4 WorldMatrix
        {
            get
            {
                var worldMatrix = Matrix4.Identity;

                if (Parent != null)
                {
                    worldMatrix = Parent.WorldTransform;
                }

                return worldMatrix;
            }
        }

        #endregion properties

        public void AddChild(Node node)
        {
            node.Parent = this;

            _children.Add(node);
        }

        public void AddRenderable(Renderable renderable)
        {
            _renderables.Add(renderable);
        }

        public virtual void Update(float clientWidth, float clientHeight)
        {
            foreach (var child in _children)
            {
                child.Update(clientWidth, clientHeight);
            }
        }

        public void Render(CameraNode camera, LightNode[] lights)
        {
            foreach (var renderable in _renderables)
            {
                renderable.Render(WorldTransform, camera.View, camera.Projection, camera, lights);
            }

            foreach (var child in _children)
            {
                child.Render(camera, lights);
            }
        }

        #endregion public

        #region private

        #region properties

        private Matrix4 LocalTransform
        {
            get
            {
                return
                    Matrix4.CreateScale(LocalScale)
                    * Matrix4.CreateFromQuaternion(LocalRotation)
                    * Matrix4.CreateTranslation(LocalPosition);
            }
        }

        private Matrix4 WorldTransform
        {
            get
            {
                return LocalTransform * WorldMatrix;
            }
        }

        #endregion properties

        #endregion private
    }
}