﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;

namespace RogueEngine.SceneManagement.Nodes
{
    public class CameraNode : Node
    {
        public CameraNode() : base()
        {
            ClearColor = Color.Black;
            ProjectionType = ProjectionTypes.Perspective;
            FieldOfView = (float)Math.PI / 4;
            OrthographicSize = new Size(1, 1);
            NearClippingPlane = 0.1f;
            FarClippingPlane = 1000.0f;
        }

        #region properties

        public Color ClearColor { get; set; }

        public ProjectionTypes ProjectionType { get; set; }

        public Matrix4 View { get; set; }

        public Matrix4 Projection { get; private set; }

        public float FieldOfView { get; set; }

        public Size OrthographicSize { get; set; }

        public float NearClippingPlane { get; set; }

        public float FarClippingPlane { get; set; }

        public RectangleF ViewPortRect { get; set; }

        public int Depth { get; set; }

        #endregion properties

        public override void Update(float clientWidth, float clientHeight)
        {
            GL.Viewport(0, 0, (int)clientWidth, (int)clientHeight);

            if (ProjectionType == ProjectionTypes.Orthographic)
            {
                Projection = Matrix4.CreateOrthographic(0, 0, OrthographicSize.Width, OrthographicSize.Height);
            }
            else if (ProjectionType == ProjectionTypes.Perspective)
            {
                Projection = Matrix4.CreatePerspectiveFieldOfView(FieldOfView, clientWidth / clientHeight, NearClippingPlane, FarClippingPlane);
            }

            var position = Position;
            var target = Position + Forward;
            var up = Up;

            View = Matrix4.LookAt(position, target, up);

            base.Update(clientWidth, clientHeight);
        }

        public void Clear()
        {
            GL.ClearColor(ClearColor);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
        }
    }
}