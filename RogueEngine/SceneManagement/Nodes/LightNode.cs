﻿using System.Drawing;

namespace RogueEngine.SceneManagement.Nodes
{
    public class LightNode : Node
    {
        public LightNode()
        {
            Color = Color.White;
            Constant = 1;
            Linear = 0.5f;
            Quadratic = 0.5f;
        }

        #region properties

        public Color Color { get; set; }

        public float Constant { get; set; }

        public float Linear { get; set; }

        public float Quadratic { get; set; }

        #endregion properties
    }
}