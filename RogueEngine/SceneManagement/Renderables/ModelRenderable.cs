﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using RogueEngine.AssetManagement.Assets;
using RogueEngine.SceneManagement.Nodes;

namespace RogueEngine.SceneManagement.Renderables
{
    public class ModelRenderable : Renderable
    {
        public Model Model { get; set; }

        public ShaderProgram Shader { get; set; }

        public override void Render(Matrix4 model, Matrix4 view, Matrix4 projection, CameraNode camera, LightNode[] lights)
        {
            GL.Enable(EnableCap.DepthTest);

            if (Model != null)
            {
                foreach (var mesh in Model.Meshes)
                {
                    if (Model.Materials.Count <= mesh.MaterialIndex || Model.Materials[mesh.MaterialIndex] == null)
                    {
                        continue;
                    }

                    var material = Model.Materials[mesh.MaterialIndex];

                    material.Shader.UseProgram();
                    material.Shader.SetUniform(material.Shader.GetUniform("Model"), model);
                    material.Shader.SetUniform(material.Shader.GetUniform("View"), view);
                    material.Shader.SetUniform(material.Shader.GetUniform("Projection"), projection);
                    material.Shader.SetUniform(material.Shader.GetUniform("CameraPosition"), camera.Position);

                    SetupLighting(material.Shader, lights);

                    for (int i = 0; i < material.Textures.Length; i++)
                    {
                        GL.ActiveTexture(TextureUnit.Texture0 + i);

                        if (material.Textures[i] != null)
                        {
                            material.Textures[i].Bind();

                            material.Shader.SetUniform(material.Shader.GetUniform("Texture" + i), i);
                        }
                        else
                        {
                            GL.BindTexture(TextureTarget.Texture2D, 0);
                        }
                    }

                    mesh.Render();
                }
            }

            GL.Disable(EnableCap.DepthTest);
        }

        private void SetupLighting(ShaderProgram shader, LightNode[] lights)
        {
            shader.SetUniform(shader.GetUniform("Ambient"), 0.005f);

            if (lights.Length > 0)
            {
                shader.SetUniform(shader.GetUniform("Lights[0].Position"), lights[0].Position);

                shader.SetUniform(shader.GetUniform("Lights[0].Color"),
                    new Vector3(
                        lights[0].Color.R / 255.0f,
                        lights[0].Color.G / 255.0f,
                        lights[0].Color.B / 255.0f));

                shader.SetUniform(shader.GetUniform("Lights[0].Constant"), lights[0].Constant);
                shader.SetUniform(shader.GetUniform("Lights[0].Linear"), lights[0].Linear);
                shader.SetUniform(shader.GetUniform("Lights[0].Quadratic"), lights[0].Quadratic);
            }
        }
    }
}