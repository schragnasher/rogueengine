﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using RogueEngine.AssetManagement.Assets;
using RogueEngine.SceneManagement.Nodes;
using System;

namespace RogueEngine.SceneManagement.Renderables
{
    internal class PrimitiveRenderable : Renderable
    {
        private int _vertexArrayObject;
        private int _vboPosition;
        private int _vboColor;
        private int _vboIndice;

        public ShaderProgram Shader { get; set; }

        public override void Render(Matrix4 model, Matrix4 view, Matrix4 projection, CameraNode camera, LightNode[] lights)
        {
            var transform = model * view * projection;

            Shader.UseProgram();

            GL.BindVertexArray(_vertexArrayObject);

            Shader.SetUniform(Shader.GetUniform("Model"), model);
            Shader.SetUniform(Shader.GetUniform("View"), view);
            Shader.SetUniform(Shader.GetUniform("Projection"), projection);
            Shader.SetUniform(Shader.GetUniform("CameraPosition"), -camera.View.ExtractTranslation());

            GL.DrawElements(BeginMode.Triangles, 3, DrawElementsType.UnsignedInt, 0);

            GL.BindVertexArray(0);
        }

        private void LoadGeometry()
        {
            var vertexData = GetVertices();

            var colorData = new Vector3[] {
                new Vector3( 1f, 0f, 0f),
                new Vector3( 0f, 0f, 1f),
                new Vector3( 0f, 1f, 0f)
            };

            var indiceData = GetIndices();

            //generate ids
            GL.GenVertexArrays(1, out _vertexArrayObject);
            GL.GenBuffers(1, out _vboPosition);
            GL.GenBuffers(1, out _vboColor);
            GL.GenBuffers(1, out _vboIndice);

            //bind the vertex array object, all of the next calls will effect this vertex array object
            GL.BindVertexArray(_vertexArrayObject);

            //tell opengl to bind the position buffer, then set the data for that buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vboPosition);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * Vector3.SizeInBytes), vertexData, BufferUsageHint.StaticDraw);

            //enable the position attribute, and tell opengl where the data is
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, 0, 0);

            //tell opengl to bind the color buffer, then set the data for that buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vboColor);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(colorData.Length * Vector3.SizeInBytes), colorData, BufferUsageHint.StaticDraw);

            //enable the position attribute, and tell opengl where the data is
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, 0);

            //load indices into buffer and enable in vertex array object
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _vboIndice);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indiceData.Length * sizeof(uint)), indiceData, BufferUsageHint.StaticDraw);

            //clear the bound buffer, so nothign funny happens
            GL.BindVertexArray(0);
        }

        private Vector3[] GetVertices()
        {
            return new Vector3[] { };
        }

        private uint[] GetIndices()
        {
            return new uint[] { };
        }
    }
}