﻿using log4net;
using OpenTK.Graphics.OpenGL;
using RogueEngine.SceneManagement.Nodes;
using RogueEngine.Utility;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace RogueEngine.SceneManagement
{
    [Export]
    public class SceneManager
    {
        private readonly ILog _log;
        private RogueGame _rogueGame;
        private Node _rootNode;
        private List<CameraNode> _cameras;
        private List<LightNode> _lights;

        public SceneManager()
        {
            _log = LogManager.GetLogger(GetType());
            _rootNode = new Node();
            _cameras = new List<CameraNode>();
            _lights = new List<LightNode>();
        }

        #region public

        public void Initialize()
        {
            _log.Debug("Scene Manager Initialized");

            _rogueGame = ServiceLocator.GetInstance<RogueGame>();
        }

        public void Update()
        {
            _rootNode.Update(_rogueGame.Width, _rogueGame.Height);
        }

        public void Render()
        {
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);

            foreach (var camera in _cameras)
            {
                camera.Clear();

                _rootNode.Render(camera, _lights.ToArray());
            }
        }

        public void AddNode(Node node)
        {
            _rootNode.AddChild(node);
        }

        public void RegisterCamera(CameraNode camera)
        {
            _cameras.Add(camera);
        }

        public void RegisterLight(LightNode light)
        {
            _lights.Add(light);
        }

        public void Shutdown()
        {
            _log.Debug("Scene Manager Shutdown");
        }

        #endregion public
    }
}