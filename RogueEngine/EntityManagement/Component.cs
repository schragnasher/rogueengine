﻿using System.Reflection;

namespace RogueEngine.EntityManagement
{
    public abstract class Component
    {
        internal Component()
        {
            IsEnabled = true;
        }

        #region public

        #region properties

        public Entity Entity { get; internal set; }

        public bool IsEnabled { get; private set; }

        #endregion properties

        public void Enable()
        {
            if (!IsEnabled)
            {
                IsEnabled = true;

                SendMessage("OnEnable");
            }
        }

        public void Disable()
        {
            if (IsEnabled)
            {
                IsEnabled = false;

                SendMessage("OnDisable");
            }
        }

        public void SendMessage(string name, object[] parameters = null)
        {
            if (Entity.IsEnabled && IsEnabled)
            {
                var method = GetType().GetMethod(name,
                    BindingFlags.Public |
                    BindingFlags.NonPublic |
                    BindingFlags.Instance);

                method?.Invoke(this, parameters);
            }
        }

        public void Destroy()
        {
            Entity.BroadcastMessage("OnDestroy");

            Shutdown();
        }

        #endregion public

        #region internal

        internal virtual void Initialize()
        {
        }

        internal virtual void Update(double frameTime)
        {
        }

        internal virtual void Shutdown()
        {
        }

        #endregion internal
    }
}