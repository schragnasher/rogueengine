﻿using log4net;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;

namespace RogueEngine.EntityManagement
{
    [Export]
    public class EntityManager
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private List<Entity> _entities;
        private List<Entity> _entitiesToAdd;

        internal EntityManager()
        {
            _entities = new List<Entity>();
            _entitiesToAdd = new List<Entity>();
        }

        #region public

        public Entity CreateEntity(string name)
        {
            var entity = new Entity(name);

            _entitiesToAdd.Add(entity);

            return entity;
        }

        public Entity FindEntityByName(string name)
        {
            return _entities.FirstOrDefault(x => x.Name.Equals(name));
        }

        public List<Entity> FindEntitiesByName(string name)
        {
            return _entities.Where(x => x.Name.Equals(name)).ToList();
        }

        #endregion public

        #region internal

        internal void Initialize()
        {
            _log.Debug("Entity Manager Initialized");
        }

        internal void Update(double frameTime)
        {
            _entities.AddRange(_entitiesToAdd);
            _entitiesToAdd.Clear();

            foreach (var entity in _entities)
            {
                entity.Update((float)frameTime);
            }
        }

        internal void Shutdown()
        {
            _log.Debug("Entity Manager Shutdown");
        }

        #endregion internal
    }
}