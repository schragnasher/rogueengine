﻿using RogueEngine.SceneManagement;
using RogueEngine.Utility;
using System.Collections.Generic;
using System.Linq;

namespace RogueEngine.EntityManagement
{
    public class Entity
    {
        private SceneManager _sceneManager;
        private List<Component> _components;
        private List<Component> _componenetsToAdd;
        private List<Component> _componenetsToRemove;

        internal Entity(string name)
        {
            Name = name;

            _sceneManager = ServiceLocator.GetInstance<SceneManager>();

            _components = new List<Component>();
            _componenetsToAdd = new List<Component>();
            _componenetsToRemove = new List<Component>();

            SceneNode = new Node();

            IsEnabled = true;

            _sceneManager.AddNode(SceneNode);
        }

        #region public

        #region properties

        public string Name { get; set; }

        public bool IsEnabled { get; private set; }

        #endregion properties

        public T AddComponent<T>() where T : Component, new()
        {
            var component = _components.OfType<T>().FirstOrDefault();

            if (component == null)
            {
                component = new T();

                component.Entity = this;

                component.Initialize();

                _componenetsToAdd.Add(component);
            }

            return component;
        }

        public T GetComponent<T>() where T : Component, new()
        {
            var component = _components.OfType<T>().FirstOrDefault();

            return component;
        }

        public void Enable()
        {
            if (!IsEnabled)
            {
                IsEnabled = true;

                BroadcastMessage("OnEnable");
            }
        }

        public void Disable()
        {
            if (IsEnabled)
            {
                IsEnabled = false;

                BroadcastMessage("OnDisable");
            }
        }

        public void BroadcastMessage(string name, object[] parameters = null)
        {
            if (IsEnabled)
            {
                foreach (Component component in _components)
                {
                    component.SendMessage(name, parameters);
                }
            }
        }

        #endregion public

        #region internal

        #region properties

        internal Node SceneNode { get; }

        #endregion properties

        internal void Update(double frameTime)
        {
            //remove destroyed components
            foreach (var component in _componenetsToRemove)
            {
                _components.Remove(component);
            }

            _componenetsToRemove.Clear();

            //add new components
            foreach (var component in _componenetsToAdd)
            {
                _components.Add(component);

                if (component.IsEnabled) component.SendMessage("OnEnable");
            }

            _componenetsToAdd.Clear();

            //update all components
            foreach (var component in _components)
            {
                component.Update(frameTime);
            }
        }

        #endregion internal
    }
}