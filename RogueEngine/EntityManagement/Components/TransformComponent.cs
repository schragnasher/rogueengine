﻿using OpenTK;

namespace RogueEngine.EntityManagement.Components
{
    public class TransformComponent : Component
    {
        #region public

        #region properties

        public Vector3 Right
        {
            get
            {
                return Entity.SceneNode.Right;
            }
        }

        public Vector3 Left
        {
            get
            {
                return Entity.SceneNode.Left;
            }
        }

        public Vector3 Up
        {
            get
            {
                return Entity.SceneNode.Up;
            }
        }

        public Vector3 Down
        {
            get
            {
                return Entity.SceneNode.Down;
            }
        }

        public Vector3 Forward
        {
            get
            {
                return Entity.SceneNode.Forward;
            }
        }

        public Vector3 Backward
        {
            get
            {
                return Entity.SceneNode.Backward;
            }
        }

        public Vector3 Position
        {
            get
            {
                return Entity.SceneNode.Position;
            }

            set
            {
                Entity.SceneNode.Position = value;
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return Entity.SceneNode.Rotation;
            }

            set
            {
                Entity.SceneNode.Rotation = value;
            }
        }

        public Vector3 Scale
        {
            get
            {
                return Entity.SceneNode.Scale;
            }

            set
            {
                Entity.SceneNode.Scale = value;
            }
        }

        public Vector3 LocalPosition
        {
            get
            {
                return Entity.SceneNode.LocalPosition;
            }

            set
            {
                Entity.SceneNode.LocalPosition = value;
            }
        }

        public Quaternion LocalRotation
        {
            get
            {
                return Entity.SceneNode.LocalRotation;
            }

            set
            {
                Entity.SceneNode.LocalRotation = value;
            }
        }

        public Vector3 LocalScale
        {
            get
            {
                return Entity.SceneNode.LocalScale;
            }

            set
            {
                Entity.SceneNode.LocalScale = value;
            }
        }

        #endregion properties

        public void AddChild(TransformComponent transform)
        {
            Entity.SceneNode.AddChild(transform.Entity.SceneNode);
        }

        public void Translate(Vector3 translation, bool isWorldSpace = false)
        {
            Translate(translation.X, translation.Y, translation.Z, isWorldSpace);
        }

        public void Translate(float x, float y, float z, bool isWorldSpace = false)
        {
            Matrix4.CreateTranslation(x, y, z, out Matrix4 translation);

            if (isWorldSpace)
            {
                Entity.SceneNode.Position = Vector3.TransformPosition(Entity.SceneNode.Position, translation);
            }
            else
            {
                Entity.SceneNode.LocalPosition = Vector3.TransformPosition(Entity.SceneNode.LocalPosition, translation);
            }
        }

        public void LookAt(Vector3 target, Vector3 up)
        {
            var forward = (target - Entity.SceneNode.Position);
            var left = Vector3.Cross(up, forward);
            var matrix = new Matrix3(left, up, forward);
            var quat = Quaternion.FromMatrix(matrix).Normalized();

            Rotation = quat;
        }

        #endregion public
    }
}