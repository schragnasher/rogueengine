﻿using RogueEngine.SceneManagement;
using RogueEngine.SceneManagement.Nodes;
using RogueEngine.Utility;

namespace RogueEngine.EntityManagement.Components
{
    public class CameraComponent : Component
    {
        private SceneManager _sceneManager;
        private CameraNode _camera;

        public CameraComponent()
        {
            _sceneManager = ServiceLocator.GetInstance<SceneManager>();

            _camera = new CameraNode();

            _sceneManager.RegisterCamera(_camera);
        }

        #region public

        internal override void Initialize()
        {
            Entity.SceneNode.AddChild(_camera);
        }

        #endregion public
    }
}