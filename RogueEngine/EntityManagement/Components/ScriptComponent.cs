﻿namespace RogueEngine.EntityManagement.Components
{
    public class ScriptComponent : Component
    {
        public ScriptComponent()
        {
        }

        #region internal

        internal bool _isAwake;
        internal bool _isStarted;

        internal override void Update(double frameTime)
        {
            if (!_isAwake)
            {
                _isAwake = true;

                SendMessage("OnAwake");
            }

            if (!_isStarted && IsEnabled)
            {
                _isStarted = true;

                SendMessage("OnStart");
            }

            if (IsEnabled)
            {
                SendMessage("OnUpdate", new object[] { frameTime });
            }
        }

        internal override void Shutdown()
        {
            SendMessage("OnDestroy");
        }

        #endregion internal
    }
}