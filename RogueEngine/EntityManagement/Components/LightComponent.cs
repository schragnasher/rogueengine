﻿using RogueEngine.SceneManagement;
using RogueEngine.SceneManagement.Nodes;
using RogueEngine.Utility;
using System.Drawing;

namespace RogueEngine.EntityManagement.Components
{
    public class LightComponent : Component
    {
        private SceneManager _sceneManager;
        private LightNode _lightNode;

        #region properties

        public Color Color
        {
            get
            {
                return _lightNode.Color;
            }

            set
            {
                _lightNode.Color = value;
            }
        }

        public float Constant
        {
            get
            {
                return _lightNode.Constant;
            }
            set
            {
                _lightNode.Constant = value;
            }
        }

        public float Linear
        {
            get
            {
                return _lightNode.Linear;
            }
            set
            {
                _lightNode.Linear = value;
            }
        }

        public float Quadratic
        {
            get
            {
                return _lightNode.Quadratic;
            }
            set
            {
                _lightNode.Quadratic = value;
            }
        }

        #endregion properties

        #region internal

        internal override void Initialize()
        {
            base.Initialize();

            _sceneManager = ServiceLocator.GetInstance<SceneManager>();

            _lightNode = new LightNode();

            Entity.SceneNode.AddChild(_lightNode);

            _sceneManager.RegisterLight(_lightNode);
        }

        #endregion internal
    }
}