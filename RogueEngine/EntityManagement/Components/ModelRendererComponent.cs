﻿using RogueEngine.AssetManagement.Assets;
using RogueEngine.SceneManagement.Renderables;

namespace RogueEngine.EntityManagement.Components
{
    public class ModelRendererComponent : Component
    {
        private ModelRenderable _renderable;

        public ModelRendererComponent()
        {
            _renderable = new ModelRenderable();
        }

        public Model Model
        {
            get
            {
                return _renderable.Model;
            }

            set
            {
                _renderable.Model = value;
            }
        }

        #region internal

        internal override void Initialize()
        {
            base.Initialize();

            Entity.SceneNode.AddRenderable(_renderable);
        }

        #endregion internal
    }
}