﻿using Noesis;
using RogueEngine.GuiManagement;
using RogueEngine.Utility;

namespace RogueEngine.EntityManagement.Components
{
    public class GuiViewComponent : ScriptComponent
    {
        private GuiManager _guiManager;

        public View View { get; set; }

        public string XamlPath { get; set; }

        #region private

        private void OnAwake()
        {
            _guiManager = ServiceLocator.GetInstance<GuiManager>();

            View = _guiManager.CreateView(XamlPath);
        }

        private void OnStart()
        {
        }

        private void OnEnable()
        {
        }

        private void OnDisable()
        {
        }

        private void OnUpdate(double dt)
        {
        }

        private void OnDestroy()
        {
        }

        #endregion private
    }
}