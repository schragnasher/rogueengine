﻿namespace RogueEngine
{
    public enum ProjectionTypes
    {
        Perspective = 0,
        Orthographic = 1
    }

    public enum ClearTypes
    {
        Color,
        Depth,
        Both,
        None
    }
}