﻿using log4net;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;

namespace RogueEngine.Utility
{
    public static class ServiceLocator
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static CompositionContainer _container;
        private static AggregateCatalog _catalog;

        public static void Initialize()
        {
            _catalog = new AggregateCatalog(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            _container = new CompositionContainer(_catalog);
        }

        public static T GetInstance<T>()
        {
            return _container.GetExportedValue<T>();
        }

        internal static void AddInstance(object instance)
        {
            CompositionBatch batch = new CompositionBatch();
            batch.AddPart(AttributedModelServices.CreatePart(instance));
            _container.Compose(batch);
        }
    }
}