﻿using log4net;
using RogueEngine.AssetManagement.Assets;
using System.Collections.Generic;
using System.Reflection;

namespace RogueEngine.Utility
{
    public class MeshBuilder
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private List<Vertex> _vertices;
        private List<uint> _indices;

        public MeshBuilder()
        {
            _vertices = new List<Vertex>();
            _indices = new List<uint>();
        }

        public int VertexCount
        {
            get
            {
                return _vertices.Count;
            }
        }

        public void AddVertex(Vertex vertex)
        {
            _vertices.Add(vertex);
        }

        public void AddTriangle(uint index0, uint index1, uint index2)
        {
            _indices.Add(index0);
            _indices.Add(index1);
            _indices.Add(index2);
        }

        public Mesh CreateMesh()
        {
            var mesh = new Mesh(_vertices.ToArray(), _indices.ToArray());

            return mesh;
        }
    }
}