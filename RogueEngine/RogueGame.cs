﻿using log4net;
using OpenTK;
using OpenTK.Graphics;
using RogueEngine.AssetManagement;
using RogueEngine.EntityManagement;
using RogueEngine.GuiManagement;
using RogueEngine.SceneManagement;
using RogueEngine.StageManagement;
using RogueEngine.Utility;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Reflection;

namespace RogueEngine
{
    [Export]
    public sealed class RogueGame : GameWindow
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private AssetManager _assetManager;
        private EntityManager _entityManager;
        private SceneManager _sceneManager;
        private GuiManager _guiManager;
        private StageManager _stageManager;
        private Stage _startStage;

        public RogueGame(Stage startStage) : base(1600, 900, new GraphicsMode(32, 24, 8, 16))
        {
            ServiceLocator.Initialize();
            ServiceLocator.AddInstance(this);

            _assetManager = ServiceLocator.GetInstance<AssetManager>();
            _entityManager = ServiceLocator.GetInstance<EntityManager>();
            _sceneManager = ServiceLocator.GetInstance<SceneManager>();
            _guiManager = ServiceLocator.GetInstance<GuiManager>();
            _stageManager = ServiceLocator.GetInstance<StageManager>();

            _startStage = startStage;

            _log.Debug("Rogue Engine Started");
        }

        #region protected

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _assetManager.Initialize();
            _entityManager.Initialize();
            _sceneManager.Initialize();
            _guiManager.Initialize();
            _stageManager.LoadStage(_startStage);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            _entityManager.Update(e.Time);
            _sceneManager.Update();
            _guiManager.Update(e.Time);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            _sceneManager.Render();

            _guiManager.Render();

            SwapBuffers();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            _assetManager.Shutdown();
            _entityManager.Shutdown();
            _sceneManager.Shutdown();
            _guiManager.Shutdown();

            _log.Debug("Rogue Engine Shutdown Complete");
        }

        #endregion protected
    }
}