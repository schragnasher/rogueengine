﻿using System.Reflection;

[assembly: AssemblyTitle("RogueEngine")]
[assembly: AssemblyDescription("A 3D Game Engine")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Joshua Baker")]
[assembly: AssemblyProduct("RogueEngine")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyTrademark("https://your.project.url/")]
[assembly: AssemblyFileVersion("0.0.0.1")]
#if DEBUG
[assembly: AssemblyInformationalVersion("0.0.0.1-alpha")]
#else
[assembly: AssemblyInformationalVersion("0.0.0.1")]
#endif