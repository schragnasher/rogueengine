#version 330

layout (location = 0) in vec3 InputPosition;
layout (location = 1) in vec3 InputNormal;
layout (location = 2) in vec2 InputTexcoord;
layout (location = 3) in vec3 InputTangent;
layout (location = 4) in vec3 InputBitangent;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

out VertexShaderOutput {
	vec3 Position;
	vec2 TexCoords;
	mat3 TBN;
} Output;

void main()
{
	mat3 normalMatrix = transpose(inverse(mat3(Model)));
	vec3 T = normalize(vec3(normalMatrix * InputTangent));
	vec3 B = normalize(vec3(normalMatrix * InputBitangent));
	vec3 N = normalize(vec3(normalMatrix * InputNormal));
	  
	Output.Position = vec3(Model * vec4(InputPosition, 1.0));
	Output.TexCoords = InputTexcoord;
	Output.TBN = mat3(T, B, N);

	gl_Position = (Projection * View * Model) * vec4(InputPosition, 1.0);
}
