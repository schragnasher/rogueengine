#version 330

layout (location = 0) in vec3 InputPosition;
layout (location = 1) in vec3 InputNormal;
layout (location = 2) in vec2 InputTexcoord;
layout (location = 3) in vec3 InputTangent;
layout (location = 4) in vec3 InputBitangent;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

out vec3 FragmentPosition;
out vec3 FragmentNormal;
out vec2 FragmentTexCoords;

void main()
{
	gl_Position = (Projection * View * Model) * vec4(InputPosition, 1.0);
  
	FragmentPosition = vec3(Model * vec4(InputPosition, 1.0));
	FragmentNormal = normalize(mat3(transpose(inverse(Model))) * InputNormal);
	FragmentTexCoords = InputTexcoord;
}
