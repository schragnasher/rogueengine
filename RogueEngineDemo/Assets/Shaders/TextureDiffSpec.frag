#version 330
#define MAX_LIGHTS 8

struct Light {
	vec3 Position;
	vec3 Direction;
	vec3 Color;
};

uniform float SpecularStrength = 2.0;
uniform sampler2D Texture0;
uniform sampler2D Texture1;
uniform sampler2D Texture2;
uniform Light Lights[MAX_LIGHTS];
uniform vec3 CameraPosition;

in vec3 FragmentPosition;
in vec4 FragmentColor;
in vec3 FragmentNormal;
in vec2 FragmentTexCoords;

out vec4 OutputColor;

void main()
{
	vec3 fragmentToCamera = normalize(CameraPosition - FragmentPosition);
	vec3 fragmentToLight = normalize(Lights[0].Position - FragmentPosition);
	vec3 reflectionVector = reflect(-fragmentToLight, FragmentNormal);

	//diffuse lighting  
	float diffuse = max(dot(FragmentNormal, fragmentToLight), 0.0);
	vec3 diffuseColor = diffuse * Lights[0].Color * 2.0;

	//texture color
	vec3 textureColor = vec3(texture(Texture0, FragmentTexCoords));

	//specular lighting  
	float specular = pow(max(0.0, dot(fragmentToCamera, reflectionVector)), SpecularStrength);
	vec3 specularColor = specular * vec3(texture(Texture1, FragmentTexCoords)) * diffuse;  
	
	OutputColor = vec4((textureColor * diffuseColor) + specularColor, 1.0);
}
