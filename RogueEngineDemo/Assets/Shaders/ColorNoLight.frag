#version 330

in vec4 FragmentColor;

out vec4 OutputColor;

void main()
{
	OutputColor = FragmentColor;
}
