#version 330
#define MAX_LIGHTS 8

struct Light {
	vec3 Position;
	vec3 Direction;
	vec3 Color;
};

uniform sampler2D Texture0;
uniform sampler2D Texture1;
uniform Light Lights[MAX_LIGHTS];

in VertexShaderOutput {
	vec3 Position;
	vec2 TexCoords;
	mat3 TBN;
} Input;

out vec4 OutputColor;

void main()
{
	vec3 TangentLightPos = Input.TBN * Lights[0].Position;
	vec3 TangentFragPos  = Input.TBN * Input.Position;
	
	vec3 normal = normalize(texture(Texture1, Input.TexCoords).rgb);
	normal = normalize(normal * 2.0 - 1.0);
	normal = normalize(Input.TBN * normal);
	
	//texture color
	vec3 textureColor = texture(Texture0, Input.TexCoords).rgb;

	//diffuse lighting  
	vec3 fragmentToLight = normalize(TangentLightPos - TangentFragPos);		
	float diffuse = max(dot(normal, fragmentToLight), 0.0);
	vec3 diffuseColor = diffuse * Lights[0].Color;
	
	OutputColor = vec4((textureColor * diffuseColor), 1.0);	
}
