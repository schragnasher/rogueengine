#version 330

uniform sampler2D Texture0;

in vec2 FragmentTexCoords;

out vec4 OutputColor;

void main()
{
	//texture color
	vec3 textureColor = vec3(texture(Texture0, FragmentTexCoords));
	
	OutputColor = vec4(textureColor, 1.0);
}
