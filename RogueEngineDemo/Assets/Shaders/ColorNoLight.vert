#version 330

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

layout (location = 0) in vec3 VertexPosition;

out vec4 FragmentColor;

void main()
{
  gl_Position = Projection * View * Model * vec4(VertexPosition, 1.0);
  
  FragmentColor = vec4(1.0, 1.0, 1.0, 1.0);
}
