#version 330
#define MAX_LIGHTS 8

struct Light {
	vec3 Position;
	vec3 Color;
	float Constant;
	float Linear;
	float Quadratic;
};

float specularStrength = 2;

uniform sampler2D Texture0;
uniform sampler2D Texture1;
uniform sampler2D Texture2;
uniform vec3 CameraPosition;
uniform Light Lights[MAX_LIGHTS];

in VertexShaderOutput {
	vec3 Position;
	vec2 TexCoords;
	mat3 TBN;
} Input;

out vec4 OutputColor;

void main()
{
	vec3 tangentViewPos = Input.TBN * CameraPosition;
	vec3 tangentLightPos = Input.TBN * Lights[0].Position;
	vec3 tangentFragPos  = Input.TBN * Input.Position;
	vec3 fragmentToLight = normalize(tangentLightPos - tangentFragPos);	
	vec3 fragmentToCamera = normalize(tangentViewPos - tangentFragPos);
	
	//attenuation
	float distance = length(fragmentToLight);
	float attenuation = 1.0 / (Lights[0].Constant + Lights[0].Linear * distance + Lights[0].Quadratic * (distance * distance));

	//texture color
	vec3 textureColor = texture(Texture0, Input.TexCoords).rgb;

	//normal mapping
	vec3 normalTexture = normalize(texture(Texture1, Input.TexCoords).rgb);
	vec3 normal = normalize(normalTexture * 2.0 - 1.0);
	normal = normalize(Input.TBN * normal);	

	//diffuse lighting  
	float diffuse = max(dot(normal, fragmentToLight), 0.0);
	vec3 diffuseColor = diffuse * attenuation * Lights[0].Color;

	//specular lighting
	vec3 specularTexture = texture(Texture2, Input.TexCoords).rgb;	
	vec3 reflectDir = reflect(-fragmentToLight, normal);
	vec3 halfwayDir = normalize(fragmentToLight + fragmentToCamera);
	float specular = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
	vec3 specularColor = specularStrength * specular * attenuation * Lights[0].Color * specularTexture;
	
	OutputColor = vec4((textureColor * diffuseColor) + specularColor, 1.0);	
}
