#version 330
#define MAX_LIGHTS 8

struct Light {
	vec3 Position;
	vec3 Direction;
	vec3 Color;
};

uniform sampler2D Texture0;
uniform Light Lights[MAX_LIGHTS];

in vec3 FragmentPosition;
in vec3 FragmentNormal;
in vec2 FragmentTexCoords;

out vec4 OutputColor;

void main()
{
	vec3 fragmentToLight = normalize(Lights[0].Position - FragmentPosition);

	//diffuse lighting  
	float diffuse = max(dot(FragmentNormal, fragmentToLight), 0.0);
	vec3 diffuseColor = diffuse * Lights[0].Color;

	//texture color
	vec3 textureColor = vec3(texture(Texture0, FragmentTexCoords));
	
	OutputColor = vec4((textureColor * diffuseColor), 1.0);
}
