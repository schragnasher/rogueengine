﻿using OpenTK;
using RogueEngine.EntityManagement.Components;
using System.Drawing;

namespace RogueEngineDemo.ViewModels
{
    public class LightControlsViewModel : GuiViewModelComponent
    {
        private int _lightColorRed;
        private int _lightColorGreen;
        private int _lightColorBlue;
        private float _lightConstant;
        private float _lightLinear;
        private float _lightQuadratic;
        private float _modelLocalX;
        private float _modelLocalY;
        private float _modelLocalZ;
        private float _modelLocalRotationX;
        private float _modelLocalRotationY;
        private float _modelLocalRotationZ;
        private float _modelPivotWorldX;
        private float _modelPivotWorldY;
        private float _modelPivotWorldZ;
        private float _modelPivotWorldRotationX;
        private float _modelPivotWorldRotationY;
        private float _modelPivotWorldRotationZ;

        #region public

        #region properties

        public TransformComponent ModelPivotTransform { get; set; }
        public TransformComponent ModelTransform { get; set; }

        public int LightColorRed
        {
            get { return _lightColorRed; }
            set { SetProperty(ref _lightColorRed, value); UpdateLight(); }
        }

        public int LightColorGreen
        {
            get { return _lightColorGreen; }
            set { SetProperty(ref _lightColorGreen, value); UpdateLight(); }
        }

        public int LightColorBlue
        {
            get { return _lightColorBlue; }
            set { SetProperty(ref _lightColorBlue, value); UpdateLight(); }
        }

        public float LightConstant
        {
            get { return _lightConstant; }
            set { SetProperty(ref _lightConstant, value); UpdateLight(); }
        }

        public float LightLinear
        {
            get { return _lightLinear; }
            set { SetProperty(ref _lightLinear, value); UpdateLight(); }
        }

        public float LightQuadratic
        {
            get { return _lightQuadratic; }
            set { SetProperty(ref _lightQuadratic, value); UpdateLight(); }
        }

        public LightComponent Light { get; set; }

        public float ModelLocalX
        {
            get { return _modelLocalX; }
            set { SetProperty(ref _modelLocalX, value); }
        }

        public float ModelLocalY
        {
            get { return _modelLocalY; }
            set { SetProperty(ref _modelLocalY, value); }
        }

        public float ModelLocalZ
        {
            get { return _modelLocalZ; }
            set { SetProperty(ref _modelLocalZ, value); }
        }

        public float ModelLocalRotationX
        {
            get { return _modelLocalRotationX; }
            set { SetProperty(ref _modelLocalRotationX, value); }
        }

        public float ModelLocalRotationY
        {
            get { return _modelLocalRotationY; }
            set { SetProperty(ref _modelLocalRotationY, value); }
        }

        public float ModelLocalRotationZ
        {
            get { return _modelLocalRotationZ; }
            set { SetProperty(ref _modelLocalRotationZ, value); }
        }

        public float ModelPivotWorldX
        {
            get { return _modelPivotWorldX; }
            set { SetProperty(ref _modelPivotWorldX, value); }
        }

        public float ModelPivotWorldY
        {
            get { return _modelPivotWorldY; }
            set { SetProperty(ref _modelPivotWorldY, value); }
        }

        public float ModelPivotWorldZ
        {
            get { return _modelPivotWorldZ; }
            set { SetProperty(ref _modelPivotWorldZ, value); }
        }

        public float ModelPivotWorldRotationX
        {
            get { return _modelPivotWorldRotationX; }
            set { SetProperty(ref _modelPivotWorldRotationX, value); }
        }

        public float ModelPivotWorldRotationY
        {
            get { return _modelPivotWorldRotationY; }
            set { SetProperty(ref _modelPivotWorldRotationY, value); }
        }

        public float ModelPivotWorldRotationZ
        {
            get { return _modelPivotWorldRotationZ; }
            set { SetProperty(ref _modelPivotWorldRotationZ, value); }
        }

        #endregion properties

        #endregion public

        #region private

        private void OnStart()
        {
            var viewComponent = Entity.GetComponent<GuiViewComponent>();

            LightColorRed = 255;
            LightColorGreen = 255;
            LightColorBlue = 255;
            Light.Constant = 1;
            LightLinear = 0.5f;
            LightQuadratic = 0.5f;

            viewComponent.View.Content.DataContext = this;
        }

        private void OnUpdate(double frameTime)
        {
            if (ModelTransform != null)
            {
                ModelTransform.LocalPosition = new Vector3(ModelLocalX, ModelLocalY, ModelLocalZ);
                ModelTransform.LocalRotation = Quaternion.FromEulerAngles(
                    MathHelper.DegreesToRadians(ModelLocalRotationX),
                    MathHelper.DegreesToRadians(ModelLocalRotationY),
                    MathHelper.DegreesToRadians(ModelLocalRotationZ));
            }

            if (ModelPivotTransform != null)
            {
                ModelPivotTransform.Position = new Vector3(ModelPivotWorldX, ModelPivotWorldY, ModelPivotWorldZ);
                ModelPivotTransform.Rotation = Quaternion.FromEulerAngles(
                    MathHelper.DegreesToRadians(ModelPivotWorldRotationX),
                    MathHelper.DegreesToRadians(ModelPivotWorldRotationY),
                    MathHelper.DegreesToRadians(ModelPivotWorldRotationZ));
            }
        }

        private void UpdateLight()
        {
            Light.Color = Color.FromArgb(LightColorRed, LightColorGreen, LightColorBlue);
            Light.Constant = LightConstant;
            Light.Linear = LightLinear;
            Light.Quadratic = LightQuadratic;
        }

        #endregion private
    }
}