﻿using OpenTK;
using RogueEngine.AssetManagement;
using RogueEngine.AssetManagement.Assets;
using RogueEngine.EntityManagement;
using RogueEngine.EntityManagement.Components;
using RogueEngine.SceneManagement;
using RogueEngine.StageManagement;
using RogueEngine.Utility;
using RogueEngineDemo.Scripts;
using RogueEngineDemo.ViewModels;

namespace RogueEngineDemo.Stages
{
    public class LoadingStage : Stage
    {
        #region public

        public override void Load()
        {
            var entityManager = ServiceLocator.GetInstance<EntityManager>();
            var assetManager = ServiceLocator.GetInstance<AssetManager>();
            var sceneManager = ServiceLocator.GetInstance<SceneManager>();

            //load textures
            var nanosuitArmDiffuse = assetManager.Load<Texture2d>("Models/Nanosuit/arm_dif.png") as Texture2d;
            var nanosuitBodyDiffuse = assetManager.Load<Texture2d>("Models/Nanosuit/body_dif.png") as Texture2d;
            var nanosuitGlassDiffuse = assetManager.Load<Texture2d>("Models/Nanosuit/glass_dif.png") as Texture2d;
            var nanosuitHandDiffuse = assetManager.Load<Texture2d>("Models/Nanosuit/hand_dif.png") as Texture2d;
            var nanosuitHelmetDiffuse = assetManager.Load<Texture2d>("Models/Nanosuit/helmet_dif.png") as Texture2d;
            var nanosuitLegDiffuse = assetManager.Load<Texture2d>("Models/Nanosuit/leg_dif.png") as Texture2d;

            var nanosuitArmNorm = assetManager.Load<Texture2d>("Models/Nanosuit/arm_showroom_ddn.png") as Texture2d;
            var nanosuitBodyNorm = assetManager.Load<Texture2d>("Models/Nanosuit/body_showroom_ddn.png") as Texture2d;
            var nanosuitGlassNorm = assetManager.Load<Texture2d>("Models/Nanosuit/glass_ddn.png") as Texture2d;
            var nanosuitHandNorm = assetManager.Load<Texture2d>("Models/Nanosuit/hand_showroom_ddn.png") as Texture2d;
            var nanosuitHelmetNorm = assetManager.Load<Texture2d>("Models/Nanosuit/helmet_showroom_ddn.png") as Texture2d;
            var nanosuitLegNorm = assetManager.Load<Texture2d>("Models/Nanosuit/leg_showroom_ddn.png") as Texture2d;

            var nanosuitArmSpec = assetManager.Load<Texture2d>("Models/Nanosuit/arm_showroom_spec.png") as Texture2d;
            var nanosuitBodySpec = assetManager.Load<Texture2d>("Models/Nanosuit/body_showroom_spec.png") as Texture2d;
            var nanosuitHandSpec = assetManager.Load<Texture2d>("Models/Nanosuit/hand_showroom_spec.png") as Texture2d;
            var nanosuitHelmetSpec = assetManager.Load<Texture2d>("Models/Nanosuit/helmet_showroom_spec.png") as Texture2d;
            var nanosuitLegSpec = assetManager.Load<Texture2d>("Models/Nanosuit/leg_showroom_spec.png") as Texture2d;

            //load shaders
            var colorNoLightShader = assetManager.Load<ShaderProgram>("Shaders/ColorNoLight") as ShaderProgram;
            var textureNoLightShader = assetManager.Load<ShaderProgram>("Shaders/TextureNoLight") as ShaderProgram;
            var textureDiffShader = assetManager.Load<ShaderProgram>("Shaders/TextureDiff") as ShaderProgram;
            var textureDiffNormShader = assetManager.Load<ShaderProgram>("Shaders/TextureDiffNorm") as ShaderProgram;
            var textureDiffNormSpecShader = assetManager.Load<ShaderProgram>("Shaders/TextureDiffNormSpec") as ShaderProgram;

            //load materials
            var sphereMaterial = new Material(colorNoLightShader);

            var nanosuitArmMaterial = new Material(textureDiffNormSpecShader);
            nanosuitArmMaterial.Textures[0] = nanosuitArmDiffuse;
            nanosuitArmMaterial.Textures[1] = nanosuitArmNorm;
            nanosuitArmMaterial.Textures[2] = nanosuitArmSpec;

            var nanosuitBodyMaterial = new Material(textureDiffNormSpecShader);
            nanosuitBodyMaterial.Textures[0] = nanosuitBodyDiffuse;
            nanosuitBodyMaterial.Textures[1] = nanosuitBodyNorm;
            nanosuitBodyMaterial.Textures[2] = nanosuitBodySpec;

            var nanosuitGlassMaterial = new Material(textureDiffNormShader);
            nanosuitGlassMaterial.Textures[0] = nanosuitGlassDiffuse;
            nanosuitGlassMaterial.Textures[1] = nanosuitGlassNorm;

            var nanosuitHandMaterial = new Material(textureDiffNormSpecShader);
            nanosuitHandMaterial.Textures[0] = nanosuitHandDiffuse;
            nanosuitHandMaterial.Textures[1] = nanosuitHandNorm;
            nanosuitHandMaterial.Textures[2] = nanosuitHandSpec;

            var nanosuitHelmetMaterial = new Material(textureDiffNormSpecShader);
            nanosuitHelmetMaterial.Textures[0] = nanosuitHelmetDiffuse;
            nanosuitHelmetMaterial.Textures[1] = nanosuitHelmetNorm;
            nanosuitHelmetMaterial.Textures[2] = nanosuitHelmetSpec;

            var nanosuitLegMaterial = new Material(textureDiffNormSpecShader);
            nanosuitLegMaterial.Textures[0] = nanosuitLegDiffuse;
            nanosuitLegMaterial.Textures[1] = nanosuitLegNorm;
            nanosuitLegMaterial.Textures[2] = nanosuitLegSpec;

            //load models
            var nanosuitModel = assetManager.Load<Model>("Models/Nanosuit/nanosuit.obj") as Model;
            nanosuitModel.Materials.Insert(0, nanosuitBodyMaterial);
            nanosuitModel.Materials.Insert(1, nanosuitArmMaterial);
            nanosuitModel.Materials.Insert(2, nanosuitBodyMaterial);
            nanosuitModel.Materials.Insert(3, nanosuitGlassMaterial);
            nanosuitModel.Materials.Insert(4, nanosuitHandMaterial);
            nanosuitModel.Materials.Insert(5, nanosuitHelmetMaterial);
            nanosuitModel.Materials.Insert(6, nanosuitLegMaterial);

            var sphereModel = assetManager.Load<Model>("Models/sphere.obj") as Model;
            sphereModel.Materials.Insert(0, sphereMaterial);

            var planeModel = assetManager.Load<Model>("Models/plane1x1.obj") as Model;
            planeModel.Materials.Insert(0, sphereMaterial);

            var axisModel = assetManager.Load<Model>("Models/axis.obj") as Model;
            axisModel.Materials.Insert(0, sphereMaterial);

            //light entity
            var lightEntity = entityManager.CreateEntity("Light");
            var lightTransform = lightEntity.AddComponent<TransformComponent>();
            var lightRenderableCompoennt = lightEntity.AddComponent<ModelRendererComponent>();
            var lightController = lightEntity.AddComponent<FreeCamera>();
            var light = lightEntity.AddComponent<LightComponent>();
            lightRenderableCompoennt.Model = sphereModel;
            lightTransform.Position = new Vector3(0, 15, 5);
            lightTransform.LookAt(new Vector3(0, 15, 0), Vector3.UnitY);

            //plane
            var planeEntity = entityManager.CreateEntity("Plane");
            var planeTransform = planeEntity.AddComponent<TransformComponent>();
            var planeRenderableComponent = planeEntity.AddComponent<ModelRendererComponent>();
            planeRenderableComponent.Model = planeModel;
            planeTransform.LocalScale = new Vector3(100, 1, 100);

            //model entity
            var modelEntity = entityManager.CreateEntity("Model");
            var modelTransform = modelEntity.AddComponent<TransformComponent>();
            var modelRenderableComponent = modelEntity.AddComponent<ModelRendererComponent>();
            modelRenderableComponent.Model = nanosuitModel;

            //model pivot
            var modelPivotEntity = entityManager.CreateEntity("ModelPivot");
            var modelPivotTransform = modelPivotEntity.AddComponent<TransformComponent>();
            modelPivotTransform.AddChild(modelTransform);

            //camera
            var cameraEntity = entityManager.CreateEntity("Camera");
            var cameraTransform = cameraEntity.AddComponent<TransformComponent>();
            var cameraComponent = cameraEntity.AddComponent<CameraComponent>();
            cameraTransform.LocalPosition = new Vector3(0, 0, 50);
            cameraTransform.LookAt(new Vector3(0, 0, 0), Vector3.UnitY);

            var axisEntity = entityManager.CreateEntity("Axis");
            var axisTransform = axisEntity.AddComponent<TransformComponent>();
            var axisRenderableComponent = axisEntity.AddComponent<ModelRendererComponent>();
            axisRenderableComponent.Model = axisModel;
            axisTransform.LocalPosition = new Vector3(0, 0, 10);
            //axisTransform.LookAt(new Vector3(0, 0, 0), Vector3.UnitY);

            //camera pivot
            var pivotEntity = entityManager.CreateEntity("CameraPivot");
            var pivotTransform = pivotEntity.AddComponent<TransformComponent>();
            var pivotRenderableComponent = pivotEntity.AddComponent<ModelRendererComponent>();
            pivotRenderableComponent.Model = axisModel;
            pivotTransform.Position = new Vector3(0, 0, 0);
            //pivotTransform.LookAt(new Vector3(0, 0, 1), Vector3.UnitY);
            pivotTransform.AddChild(axisTransform);

            //create gui
            var guiEntity = entityManager.CreateEntity("Gui");
            var guiViewComponent = guiEntity.AddComponent<GuiViewComponent>();
            var guiViewModelComponent = guiEntity.AddComponent<LightControlsViewModel>();
            guiViewComponent.XamlPath = "Assets/Xaml/Test.xaml";
            guiViewModelComponent.ModelPivotTransform = pivotTransform;
            guiViewModelComponent.ModelTransform = modelTransform;
            guiViewModelComponent.Light = light;
        }

        #endregion public
    }
}