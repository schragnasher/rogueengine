﻿using OpenTK;
using OpenTK.Input;
using RogueEngine.EntityManagement;
using RogueEngine.EntityManagement.Components;
using System;

namespace RogueEngineDemo.Scripts
{
    public class MouseOrbitCamera : ScriptComponent
    {
        private TransformComponent _transform;
        private TransformComponent _parentTransform;
        private MouseState _previousMouseState;
        private Vector3 _localRotation;

        public TransformComponent PivotTransform { get; set; }
        public float MouseSensitivity { get; set; }
        public float CameraDistance { get; set; }
        public float ZoomSensitivity { get; set; }
        public float ZoomDampening { get; set; }
        public float OrbitDampening { get; set; }

        private void OnStart()
        {
            _transform = Entity.GetComponent<TransformComponent>();

            MouseSensitivity = 4.0f;
            CameraDistance = 10.0f;
            ZoomSensitivity = 2.0f;
            ZoomDampening = 6.0f;
            OrbitDampening = 10.0f;
        }

        private float Lerp(float firstFloat, float secondFloat, float by)
        {
            return firstFloat * (1 - by) + secondFloat * by;
        }

        private void OnUpdate(double dt)
        {
            var currentMouseState = Mouse.GetState();
            var wheelDelta = _previousMouseState.WheelPrecise - currentMouseState.WheelPrecise;
            var xDelta = _previousMouseState.X - currentMouseState.X;
            var yDelta = _previousMouseState.Y - currentMouseState.Y;

            if (wheelDelta != 0)
            {
                var scrollAmount = wheelDelta * ZoomSensitivity;

                //zoom faster as i get farther away
                scrollAmount *= CameraDistance * 0.3f;

                CameraDistance += scrollAmount;
                CameraDistance = MathHelper.Clamp(CameraDistance, 1.5f, 100.0f);
            }

            //set position
            _transform.LocalPosition = new Vector3(
                _transform.LocalPosition.X,
                _transform.LocalPosition.Y,
                Lerp(_transform.LocalPosition.Z, CameraDistance, (float)dt * ZoomDampening));

            //if (xDelta != 0 || yDelta != 0)
            //{
            //    _localRotation.X += xDelta * MouseSensitivity;
            //    _localRotation.Y += yDelta * MouseSensitivity;

            //    _localRotation.Y = MathHelper.Clamp(_localRotation.Y, 0.0f, 90.0f);
            //}

            //var quat = Quaternion.FromEulerAngles(
            //    MathHelper.DegreesToRadians(_localRotation.Y),
            //    MathHelper.DegreesToRadians(_localRotation.X),
            //    0);
            //PivotTransform.Rotation = quat;

            _previousMouseState = currentMouseState;
        }
    }
}