﻿using OpenTK;
using OpenTK.Input;
using RogueEngine.EntityManagement.Components;

namespace RogueEngineDemo.Scripts
{
    public class FreeCamera : ScriptComponent
    {
        private float _slowCameraSpeed = 0.1f;
        private TransformComponent _transform;

        private void OnAwake()
        {
            _transform = Entity.GetComponent<TransformComponent>();

            //_transform.Position = new Vector3(0, 0, 10);

            //_transform.LookAt(new Vector3(0, 0, 0), Vector3.UnitY);
        }

        private void OnUpdate(double frameTime)
        {
            if (_transform != null)
            {
                var currentKeyboardState = Keyboard.GetState();

                if (currentKeyboardState.IsKeyDown(Key.W))
                {
                    _transform.Position += _transform.Forward * _slowCameraSpeed;
                }

                if (currentKeyboardState.IsKeyDown(Key.S))
                {
                    _transform.Position += _transform.Backward * _slowCameraSpeed;
                }

                if (currentKeyboardState.IsKeyDown(Key.A))
                {
                    _transform.Position += _transform.Left * _slowCameraSpeed;
                }

                if (currentKeyboardState.IsKeyDown(Key.D))
                {
                    _transform.Position += _transform.Right * _slowCameraSpeed;
                }

                if (currentKeyboardState.IsKeyDown(Key.Q))
                {
                    _transform.Position += _transform.Up * _slowCameraSpeed;
                }

                if (currentKeyboardState.IsKeyDown(Key.E))
                {
                    _transform.Position += _transform.Down * _slowCameraSpeed;
                }
            }
        }
    }
}