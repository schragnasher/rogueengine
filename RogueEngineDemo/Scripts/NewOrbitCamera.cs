﻿using OpenTK;
using OpenTK.Input;
using RogueEngine.EntityManagement.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RogueEngineDemo.Scripts
{
    public class NewOrbitCamera : ScriptComponent
    {
        private TransformComponent _transform;
        public TransformComponent _target;
        public float distance = 5.0f;
        public float xSpeed = 0.01f;
        public float ySpeed = 0.01f;
        public float yMinLimit = -20f;
        public float yMaxLimit = 80f;
        public float distanceMin = .5f;
        public float distanceMax = 15f;
        private float x = 0.0f;
        private float y = 0.0f;

        private MouseState _previousMouseState;

        private void OnStart()
        {
            _transform = Entity.GetComponent<TransformComponent>();
        }

        private void OnUpdate(double dt)
        {
            if (_target != null)
            {
                var currentMouseState = Mouse.GetState();
                var wheelDelta = _previousMouseState.WheelPrecise - currentMouseState.WheelPrecise;
                var xDelta = _previousMouseState.X - currentMouseState.X;
                var yDelta = _previousMouseState.Y - currentMouseState.Y;

                x += xDelta * xSpeed * distance * 0.02f;
                y -= yDelta * ySpeed * 0.02f;

                y = ClampAngle(y, yMinLimit, yMaxLimit);

                var rotation = Quaternion.FromEulerAngles(y, x, 0);

                distance = MathHelper.Clamp(distance - wheelDelta * 5, distanceMin, distanceMax);

                var negDistance = new Vector3(0.0f, 0.0f, -distance);
                var position = rotation * negDistance + _target.Position;

                _transform.Rotation = rotation;
                _transform.Position = position;

                _previousMouseState = currentMouseState;
            }
        }

        public static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360F)
                angle += 360F;
            if (angle > 360F)
                angle -= 360F;
            return MathHelper.Clamp(angle, min, max);
        }
    }
}