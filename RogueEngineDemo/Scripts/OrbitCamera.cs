﻿using OpenTK;
using RogueEngine.EntityManagement.Components;

namespace RogueEngineDemo.Scripts
{
    public class OrbitCamera : ScriptComponent
    {
        private TransformComponent _transform;

        public OrbitCamera()
        {
            Target = Vector3.Zero;
            Distance = 1;
        }

        #region public

        #region properties

        public Vector3 Target { get; set; }

        public float Distance { get; set; }

        #endregion properties

        #endregion public

        #region private

        private void OnAwake()
        {
            _transform = Entity.GetComponent<TransformComponent>();
        }

        private void OnUpdate(double frameTime)
        {
            if (_transform != null)
            {
                _transform.Position = (_transform.Position.Normalized() * Distance) + _transform.Right * 0.1f;

                _transform.LookAt(Target, Vector3.UnitY);
            }
        }

        #endregion private
    }
}