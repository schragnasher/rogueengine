﻿using RogueEngine;
using RogueEngineDemo.Stages;

namespace RogueEngineDemo
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            using (RogueGame rogueGame = new RogueGame(new LoadingStage()))
            {
                rogueGame.Run(30);
            }
        }
    }
}